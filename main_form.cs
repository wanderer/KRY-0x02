﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KRY_0x02
{
    public partial class main_form : Form
    {
        public main_form()
        {
            InitializeComponent();
        }

        public int im_h = 0;
        public int im_w = 0;
        public static int cap = 0;
        public string path = "";

        public int get_capacity(int a, int b)
        {
            int c = 0;
            a = im_h;
            b = im_w;
            c = ((a * b) / 8) - 1;
            cap = c;
            return c;
        }


        class do_a_stego
        {
            protected Bitmap my_bitmap;
            Color pixel;
            private readonly BackgroundWorker _bw = new BackgroundWorker();

            byte bit_array_to_byte(BitArray bit_array)
            {
                byte[] byte_array = new byte[1];
                bit_array.CopyTo(byte_array, 0);
                return byte_array[0];
            }
            public byte[] str_to_revbin_array(string input)
            {
                byte[] bA = Encoding.ASCII.GetBytes(input);
                byte[] bAA = new byte[bA.Length * 8];
                BitArray b_a = new BitArray(bA);
                for (int i = 0; i < b_a.Length; i++)
                {
                    if (b_a[i] == false)
                    {
                        bAA[i] = 0;
                    }
                    else
                    {
                        bAA[i] = 1;
                    }
                }
                return bAA;
            }

            public Bitmap encode(Bitmap b, string m)
            {
                if ((b.Height * b.Width) < 8)
                {
                    MessageBox.Show("cannot use a pic of this size bro, sorry");
                    return new Bitmap(1, 1);
                }
                else
                {
                    my_bitmap = b;
                    string message = m;
                    int counter = 0;
                    int counterzero = 0;
                    byte[] letters_to_code = str_to_revbin_array(message);
                    string zero = "00001100";


                    for (int i = 0; i < my_bitmap.Height - 1; i++)
                    {
                        if (counterzero > 7)
                        {
                            break;
                        }

                        for (int j = 0; j < my_bitmap.Width - 1; j++)
                        {
                            string blue_str = "";
                            string blue_str_modded = "";
                            int blue_int_modded = 0;
                            if ((counter < (my_bitmap.Height * my_bitmap.Width) - 8) && counter < letters_to_code.Length)
                            {
                                pixel = my_bitmap.GetPixel(j, i);
                                blue_str = Convert.ToString(pixel.B, 2).PadLeft(8, '0');
                                blue_str_modded = blue_str.Substring(0, 7) + Convert.ToString(letters_to_code[counter]);
                                blue_int_modded = Convert.ToInt32(blue_str_modded, 2);
                                Console.WriteLine($"blueSTRmodded {blue_str_modded}");
                                Console.WriteLine($"blueINTmodded {blue_int_modded}");
                                my_bitmap.SetPixel(j, i, Color.FromArgb(pixel.R, pixel.G, blue_int_modded));
                                Console.WriteLine(my_bitmap.GetPixel(i, j));
                                counter++;
                            }
                            else
                            {
                                pixel = my_bitmap.GetPixel(j, i);
                                blue_str = Convert.ToString(pixel.B, 2).PadLeft(8, '0');
                                blue_str_modded = blue_str.Substring(0, 7) + zero[counterzero];
                                blue_int_modded = Convert.ToInt32(blue_str_modded, 2);
                                Console.WriteLine($"0X00 bytes blueSTRmodded {blue_str_modded}");
                                Console.WriteLine($"0X00 bytes blueINTmodded {blue_int_modded}");
                                my_bitmap.SetPixel(j, i, Color.FromArgb(pixel.R, pixel.G, blue_int_modded));
                                counterzero++;

                                if (counterzero > 7)
                                {
                                    break;
                                }
                            }
                        }
                    }
                    return my_bitmap;
                }
            }

            public string gimme(Bitmap b)
            {
                my_bitmap = b;
                string output = "";
                bool[] my_bool_array = new bool[(cap * 8) + 8];

                int int_value;
                int counter = 0;

                for (int i = 0; i <= b.Height - 1; i++)
                {
                    for (int j = 0; j <= b.Width - 1; j++)
                    {
                        pixel = b.GetPixel(j, i);
                        int_value = pixel.B;

                        if ((int_value & 1) == 0)
                        {
                            my_bool_array[counter] = false;
                        }
                        else if ((int_value & 1) == 1)
                        {
                            my_bool_array[counter] = true;
                        }
                        counter++;
                    }
                }

                for (int i = 0; i < my_bool_array.Length / 8; i++)
                {
                    BitArray my_bit_array = new BitArray(my_bool_array.Skip(i * 8).Take(8).ToArray());
                    BitArray testifzero = new BitArray(8);
                    byte[] testifzeroarray = new byte[1];
                    for (int j = 0; j < 8; j++)
                    {
                        testifzero[j] = my_bit_array[my_bit_array.Length - j - 1];
                    }
                    testifzero.CopyTo(testifzeroarray, 0);
                    // little endian literal binary ascii 0
                    byte[] littleendianliteralbinaryasciizero = Encoding.UTF8.GetBytes("\u000C");
                    if (testifzeroarray[0] == littleendianliteralbinaryasciizero[0])
                    {
                        break;
                    }
                    char my_char = Convert.ToChar(Convert.ToInt32(bit_array_to_byte(my_bit_array)));
                    Console.WriteLine(my_char);
                    output += Convert.ToString(my_char);
                }
                // _bw.DoWork += do_magic;
                //mm.progressBar1.Maximum = my_bitmap.Height * my_bitmap.Width;
                //mm.progressBar1.Show();
                //_bw.RunWorkerAsync();
                //if (main_output != "")
                //{
                //    output = main_output;
                //}
                return output;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            // put
            if (pictureBox1.Image == null)
            {
                MessageBox.Show("you've nowhere to write to, boi");
                return;
            }
            do_a_stego do_A_Stego = new do_a_stego();
            Bitmap b = new Bitmap(pictureBox1.Image);
            string m = textBox1putm.Text;
            pictureBox1.Image = do_A_Stego.encode(b, m);
            MessageBox.Show("success!");
        }
        private void button2_Click(object sender, EventArgs e)
        {
            // get
            if (pictureBox1.Image == null)
            {
                MessageBox.Show("you've nowhere to read from, boi");
                return;
            }
            do_a_stego do_A_Stego = new do_a_stego();
            Bitmap b = new Bitmap(pictureBox1.Image);
            textBox2getm.Text = do_A_Stego.gimme(b);
            MessageBox.Show("success!", "done");
        }


        private void button4_Click(object sender, EventArgs e)
        {
            /*
             * Wrap the creation of the OpenFileDialog instance in a using statement,
             * rather than manually calling the Dispose method to ensure proper disposal
             */

            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "pick a pic";
                dlg.Filter = "custom (*.*)|*.*|png (*.png)|*.png|bmp (*.bmp)|*.bmp|tiff (*.tiff)|*.tiff";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    label5pathtopic.Text = @"path/to/pic";
                    pictureBox1.Image = new Bitmap(dlg.FileName);
                    pictureBox1.SizeMode = PictureBoxSizeMode.Normal;
                    textBoxsizemode.Text = "norm";
                    textBox3.Text = dlg.FileName;
                    path = textBox3.Text;
                    im_h = pictureBox1.Image.Height;
                    im_w = pictureBox1.Image.Width;
                    if (im_h < 100 || im_w < 100)
                    {
                        pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
                        textBoxsizemode.Text = "zoom";
                    }
                    textBox4height.Text = im_h.ToString();
                    textBox5width.Text = im_w.ToString();
                    textBox6capacity.Text = get_capacity(im_h, im_w).ToString();
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            /*/* use-default-pic button */
            string wd = Environment.CurrentDirectory;
            Console.WriteLine("* [debug] working directory: " + wd);
            string projectDirectory = Directory.GetParent(wd).Parent.FullName;
            Console.WriteLine("* [debug] project directory: " + projectDirectory);
            string filename = projectDirectory + @"/security.png";
            Console.WriteLine("* [debug] filename: " + filename);
            pictureBox1.Image = new Bitmap(filename);
            pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
            textBoxsizemode.Text = "auto";
            label5pathtopic.Text = $"Security:  Actual actual reality: nobody cares about his secrets."
                + " (Also, I would be hard-pressed to find that wrench for $5.)";
            textBox3.Text = filename + "\t\t by randall ... https://xkcd.com/538";
            path = filename;
            im_h = pictureBox1.Image.Height;
            im_w = pictureBox1.Image.Width;
            textBox4height.Text = im_h.ToString();
            textBox5width.Text = im_w.ToString();
            textBox6capacity.Text = get_capacity(im_h, im_w).ToString();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            /*/**//**///* clear pic selection */
            label5pathtopic.Text = @"path/to/pic";
            pictureBox1.Image = null;
            textBox3.Text = "";
            path = "";
            textBox2getm.Text = "";
            textBox4height.Text = "";
            textBox5width.Text = "";
            textBox6capacity.Text = "";
            textBoxsizemode.Text = "";
            im_h = 0;
            im_w = 0;
            cap = 0;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            /* save file */
            if (pictureBox1.Image == null)
            {
                MessageBox.Show("you've nothing to export, boi");
                return;
            }
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            saveFileDialog1.Title = "export your neatly stego-ed pic";
            saveFileDialog1.CheckFileExists = false;
            saveFileDialog1.CheckPathExists = false;
            saveFileDialog1.DefaultExt = "";
            saveFileDialog1.Filter = "png (*.png)|*.png|any file you wish (*.*)|*.*";
            saveFileDialog1.FilterIndex = 2;
            saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string name = saveFileDialog1.FileName;
                Console.WriteLine($"attempted to save to: {name}");
                pictureBox1.Image.Save($"{name}", ImageFormat.MemoryBmp);
                MessageBox.Show($"saved to {name}", "gj, file saved!");
            }
        }

        private void textBox1putm_TextChanged(object sender, EventArgs e)
        {
            if (Regex.IsMatch(textBox1putm.Text, "0"))
            {
                textBox1putm.BackColor = Color.PaleVioletRed;
                MessageBox.Show("null character is forbidden, sorry..\nmeaning it's going to be replaced", "just a note");
                textBox1putm.Text = Regex.Replace(textBox1putm.Text, "0", "nullchar");
                textBox1putm.Select(textBox1putm.Text.Length, 0);
                textBox1putm.BackColor = Color.White;
                return;
            }
        }
    }
}
